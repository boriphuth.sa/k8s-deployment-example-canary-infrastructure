# K8s Deployment Example Canary Infrastructure

Simple example of manual Kubernetes Canary Deployments.

This uses: https://gitlab.com/boriphuth.sa/k8s-deployment-example-app

Medium article: https://medium.com/@wuestkamp/kubernetes-canary-deployment-1-gitlab-ci-518f9fdaa7ed?source=friends_link&sk=4f3b424099f4f973f634bda75e397254


https://medium.com/@aliartiza75/gitlab-integration-with-kubernetes-46c2473a4396

cat ~/.kube/config | base64 > config.txt

## Perform initial Deployment
```
$ kubectl get all

deploy.yaml ==>replicas: 10
deploy-canary.yaml ==> replicas: 0

deployment.apps/app  10/10
while true; do curl -s 192.168.1.212:30908/| grep label; sleep 0.1; done
  "label": "v1"
  "label": "v1"
  "label": "v1"
  "label": "v1"
  "label": "v1"
  "label": "v1"
  "label": "v1"
  "label": "v1"
  "label": "v1"
  "label": "v1"
```

## Perform a Canary Deployment
## Step 1: release new version to subset of users
```
deploy.yaml replicas: 9
deploy-canary.yaml replicas: 1

  "label": "VERSION2"
  "label": "v1"
  "label": "v1"
  "label": "v1"
  "label": "v1"
  "label": "v1"
  "label": "v1"
  "label": "v1"
  "label": "v1"
  "label": "v1"
  "label": "v1"
  "label": "v1"
  "label": "v1"
  "label": "v1"

```

## Step 2: release new version to all users
```
deploy.yaml replicas: 0
deploy-canary.yaml replicas: 10
```